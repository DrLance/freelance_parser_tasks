import traceback
import requests
import psycopg2
from psycopg2.extras import DictCursor
from bs4 import BeautifulSoup
from datetime import datetime
from settings import *

start_time = datetime.now()
TYPE_FL = 1

try:
    conn = psycopg2.connect(DATABASE_DSN)
    cursor = conn.cursor(cursor_factory=DictCursor)

    pages = [1,2]

    for page in pages:
        url = "https://freelancehunt.ru/projects?page={0}".format(page)
        uri = ""
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        }
        html = requests.get(url + uri, headers=headers)
        soup = BeautifulSoup(html.text, "html.parser")
        project_list = soup.find('table', {'class': 'project-list'}).find("tbody").find_all("tr")

        for job_item in project_list:
            job_url = job_item.find("a").get("href")
            job_title = job_item.find("a").get_text()

            cursor.execute("select * from parsed_data where url = %s and type_id = %s", (job_url, TYPE_FL))
            contract_find = cursor.fetchone()

            if not contract_find:

                html_desc = requests.get(job_url, headers=headers)
                soup_desc = BeautifulSoup(html_desc.text, "html.parser")
                job_description = ""

                if soup_desc.find("span", {"data-freelancehunt-selector": "description"}):
                    job_description = soup_desc.find("span", {"data-freelancehunt-selector": "description"}).get_text()

                job_tags = []

                if soup_desc.find("div", {"class":"smaller" }):
                    job_tags = soup_desc.find("div", {"class":"smaller" }).find_all("a")

                job_categories = ",".join(job_tag.get_text() for job_tag in job_tags)

                cursor.execute("""insert into parsed_data (type_id, title, url, description, category_name, date_published_at, created_at)
                VALUES (%s, %s,%s, %s,%s, %s, %s) 
                """, (TYPE_FL, job_title.strip(), job_url, job_description.strip(), job_categories, "now()", "now()"))

    elapsed_time = (datetime.now() - start_time).total_seconds()

    conn.commit()

    print(elapsed_time)

except:
    text = traceback.format_exc()
    print(text)
    raise
