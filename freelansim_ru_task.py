import traceback
import requests
import psycopg2
from psycopg2.extras import DictCursor
from bs4 import BeautifulSoup
from datetime import datetime
from settings import *

start_time = datetime.now()
TYPE_FL = 5

try:
    conn = psycopg2.connect(DATABASE_DSN)
    cursor = conn.cursor(cursor_factory=DictCursor)
    url = "https://freelansim.ru"
    uri = "/tasks"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
    }
    html = requests.get(url + uri, headers=headers)
    soup = BeautifulSoup(html.text, "html.parser")
    project_list = soup.find('ul', {'id': 'tasks_list'}).find_all("li", {"class": "content-list__item"})



    for job_item in project_list:
        job_url = job_item.find("a").get("href")

        cursor.execute("select * from parsed_data where url = %s and type_id = %s", (url + job_url, TYPE_FL))
        contract_find = cursor.fetchone()

        if not contract_find:
            job_title = job_item.find("div", {"class": "task__title"}).get_text()
            html_desc = requests.get(url + job_url, headers=headers)
            soup_desc = BeautifulSoup(html_desc.text, "html.parser")
            job_description = soup_desc.find("div", {"class": "task__description"}).get_text()
            job_tags = job_item.find_all("li", {"class": "tags__item"})
            job_categories = ",".join(job_tag.get_text() for job_tag in job_tags)

            cursor.execute("""insert into parsed_data (type_id, title, url, description, category_name, date_published_at, created_at)
            VALUES (%s, %s,%s, %s,%s, %s, %s) 
            """, (TYPE_FL, job_title.strip(), url + job_url, job_description.strip(), job_categories, "now()", "now()"))

    elapsed_time = (datetime.now() - start_time).total_seconds()

    conn.commit()

    print(elapsed_time)

except:
    text = traceback.format_exc()
    print(text)
    raise
